import firebase from "../firebase";

const db = firebase.database().ref("/record");

class UserService {
  getAll() {
    return db;
  }

  create(user) {
    return db.push(user);
  }

}

export default new UserService();
