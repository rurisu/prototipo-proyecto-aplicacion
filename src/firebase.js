import * as firebase from "firebase";
import "firebase/database";

var config = {
  apiKey: "AIzaSyB74O-f07tFYXU6Yg5ms8sYQVS61BUGF3o",
  authDomain: "inventario-29cba.firebaseapp.com",
  projectId: "inventario-29cba",
  storageBucket: "inventario-29cba.appspot.com",
  messagingSenderId: "371077831711",
  appId: "1:371077831711:web:c52719a90f02a2c75b492a",
  measurementId: "G-MT8PZD45B8"
};


firebase.initializeApp(config);

export default firebase;